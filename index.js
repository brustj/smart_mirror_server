const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io').listen(http);
const bodyParser = require('body-parser');
const piTemp = require('pi-temperature');
const si = require('systeminformation');
const onoff = require('onoff');
const Gpio = onoff.Gpio;
// const { exec } = require('child_process');
// const PiCamera = require('pi-camera');
// const myCamera = new PiCamera({
//   mode: 'photo',
//   output: `${ __dirname }/test.jpg`,
//   width: 640,
//   height: 480,
//   nopreview: true,
// });

const pirSensor = new Gpio(27, 'in', 'both');
const lightSensor = new Gpio(7, 'in', 'both');
// let displayTimeout;

const port = 8080;
let motionFlag = false;
// let displayIsOn = true;

app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended: false }) );
app.get( '/', (req, res) => {
  return res.send( `Hello from Smart Mirror!` );
});

const setup = () => {
	// check pir motin sensor every 0.5s
	// setInterval( getMotion, 500);

	// get the rpi temp every 30s
	setInterval( getTemp, 30000 );

	// get the rpi cpu every 1s
	setInterval( getCpu, 1000 );

	// check light intensity every 1s
	setInterval( getLight, 1000);

	// initial calls
	getTemp();
	getCpu();

	// myCamera.snap()
	// 	.then((result) => {
	// 		console.log( 'webcam success', result );
	// 	})
	// 	.catch((error) => {
	// 		console.log( 'webcam error', error );
	// 	});
};

const getTemp = () => {
	piTemp.measure((err, temp) => {
		if (err) {
			console.log( 'temp error', err );

			return;
		}

		console.log( 'temp', temp );
		io.emit('smart_mirror_system', { temp });
	});
};

const getCpu = () => {
	si.currentLoad()
    .then(data => {
			console.log( 'cpu', Math.round(data.currentload) );
			io.emit('smart_mirror_system', { cpu: data.currentload });
		})
    .catch(err => {
			console.log( 'cpu error', err );
		});
};

const getMotion = () => {
	const motion = pirSensor.readSync();
	
	if (!motionFlag && motion) {
		motionFlag = true;

		// clearTimeout(displayTimeout);

		console.log( 'motion detected' );

		io.emit('smart_mirror_motion', true);
		
		// if (!displayIsOn) {
		// 	exec('tvservice -p && sudo chvt 6 && sudo chvt 7');
		// 	displayIsOn = true;
		// }
	} else if (motionFlag && !motion) {
		motionFlag = false;

		console.log( 'no motion' );

		io.emit('smart_mirror_motion', false);

		// displayTimeout = setTimeout(() => {
		// 	console.log( 'turn off display' );
		// 	displayIsOn = false;
		// 	exec('tvservice -o');
		// }, 30000);
	}

	// io.emit('smart_mirror_motion', true);
};

const getLight = () => {
	const light = lightSensor.readSync();

	console.log('light detected', light)
};

http.listen(port, () => {
	console.log( `Magic happens on port ${port}` );

	setup();
});